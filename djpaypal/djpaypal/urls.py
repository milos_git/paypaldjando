from django.contrib import admin
from django.urls import path
from applications.tienda import views

urlpatterns = [
    path('', views.index, name='index'),
    path('create_order/', views.create_paypal_order, name='create_order'),
    path('capture_order/', views.capture_paypal_order, name='capture_order'),
    # Add other paths as needed for your application
]