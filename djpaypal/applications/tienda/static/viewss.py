from django.shortcuts import render
import json
from django.http import JsonResponse
from .functions import create_order, capture_order
from django.views.decorators.clickjacking import xframe_options_sameorigin

def index(request):
    return render(request, 'index.html')

@xframe_options_sameorigin
def create_paypal_order(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            print('Received JSON data:', data)
            value = request.POST.get('value')  
            print('Value!!:', data)
            response = create_order(value)
            return JsonResponse(response, safe=False) #value is not a dict , so safe=False is needed.
        except Exception as e:
            return JsonResponse({'error': str(e)})
    else:
        return JsonResponse({'error': 'Invalid request method'})

@xframe_options_sameorigin
def capture_paypal_order(request):
    if request.method == 'POST':
        try:
            order_id = request.POST.get('orderID')  # Adjust this based on your frontend data
            response = capture_order(order_id)
            return JsonResponse(response)
        except Exception as e:
            return JsonResponse({'error': str(e)})
    else:
        return JsonResponse({'error': 'Invalid request method'})
