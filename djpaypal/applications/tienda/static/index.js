document.addEventListener('DOMContentLoaded', function() {
  // Place your entire JavaScript code here

  const beer3Button = document.getElementById("donate-3");
  const beer5Button = document.getElementById("donate-5");
  const beer10Button = document.getElementById("donate-10");

  const amountInput = document.getElementById("amount-input");
  const totalAmount = document.getElementById("total-amount");
  
  let beerCount = 0;
  let paypalButtons = null; // Initialize the variable to store the PayPal buttons instance
  let orderDatax = null; // Variable to store the order data globally

  function resultMessage(message) {
    const container = document.querySelector("#result-message");
    container.innerHTML = message;
  }


  // Function to destroy the PayPal button container
  function destroyPayPalButtonContainer() {
      const container = document.getElementById('paypal-button-container');
      
      while (container.firstChild) {
          container.removeChild(container.firstChild);
      }
  }

  // Function to initialize PayPal Button with the selected amount
  function initializePayPalButton(amount) {
      destroyPayPalButtonContainer(); // Destroy existing PayPal button container, this ensures that there is only one instance of the payPal button
      
      window.paypal.Buttons({
          style: {
              shape: 'rect',
              layout: 'vertical',
              color: 'gold',
              label: 'paypal'
          },
          async createOrder() {

              try {
                                      
                  const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
                  
                  // let order_data_json = {
                  //     'intent': 'CAPTURE',
                  //     'purchase_units': [{
                  //         'amount': {
                  //             'currency_code': 'USD',
                  //             'value': amount.toString(),
                              
                  //         }
                  //     }]
                  // };
                  // const data = JSON.stringify(order_data_json)

                  const response = await fetch('/create_order/', {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                    'X-CSRFToken': csrftoken 
                  },
                  // use the "body" param to optionally pass additional order information
                  // like product ids and quantities
                  body: JSON.stringify({
                      
                      value: [
                          {
                            id: "someid",
                            quantity: 1,
                            value: amount.toString(),
                          },
                        ],
                  }),
                });
        
                const orderDatax = await response.json();

                // console.log('orderDatax !!!', orderDatax);
        
                if (orderDatax) {
                  console.log('orderDatax !!!', orderDatax);
                  return orderDatax;

                }
                const errorDetail = orderDatax?.details?.[0];
                const errorMessage = errorDetail
                  ? `${errorDetail.issue} ${errorDetail.description} (${orderDatax.debug_id})`
                  : JSON.stringify(orderDatax);
        
                throw new Error(errorMessage);
              } catch (error) {
                console.error(error);
                // resultMessage(`Could not initiate PayPal Checkout...<br><br>${error}`);
              }
            },
            async onApprove(data, actions) {
              console.log('onApprove function executed');
              try {

                  const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;

          

                  console.log('orderDatax !!!', orderDatax);
                  console.log('data.orderID!!!',data.orderID);

                  const response = await fetch('/capture_order/', {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                    'X-CSRFToken': csrftoken 
                  },
                  body: JSON.stringify({ orderID: data.orderID }),
                });
        
                const orderData = await response.json();
                console.log('order_Data_capture: ', orderData)
                // Three cases to handle:
                //   (1) Recoverable INSTRUMENT_DECLINED -> call actions.restart()
                //   (2) Other non-recoverable errors -> Show a failure message
                //   (3) Successful transaction -> Show confirmation or thank you message
        
                const errorDetail = orderData?.details?.[0];
        
                if (errorDetail?.issue === "INSTRUMENT_DECLINED") {
                  // (1) Recoverable INSTRUMENT_DECLINED -> call actions.restart()
                  // recoverable state, per
                  // https://developer.paypal.com/docs/checkout/standard/customize/handle-funding-failures/
                  return actions.restart();
                } else if (errorDetail) {
                  // (2) Other non-recoverable errors -> Show a failure message
                  throw new Error(`${errorDetail.description} (${orderData.debug_id})`);
                } else if (!orderData.purchase_units) {
                  throw new Error(JSON.stringify(orderData));
                } else {
                  // (3) Successful transaction -> Show confirmation or thank you message
                  // Or go to another URL:  actions.redirect('thank_you.html');
                  const transaction =
                    orderData?.purchase_units?.[0]?.payments?.captures?.[0] ||
                    orderData?.purchase_units?.[0]?.payments?.authorizations?.[0];
                  resultMessage(
                    `Transaction ${transaction.status}: ${transaction.id}<br>
                  <br>See console for all available details`
                  );
                  console.log(
                    "Capture result",
                    orderData,
                    JSON.stringify(orderData, null, 2)
                  );
                  

                  window.open('https://www.sandbox.paypal.com', '_blank'); // Open PayPal in a new tab
                  // Delay redirect to your website
                  setTimeout(() => {
                    window.location.href = 'https://vadevia.com';
                    }, 5000); // Adjust the delay time as needed (5000 = 5 seconds)
    


                }

              } catch (error) {
                console.error(error);
                resultMessage(
                  `Sorry, your transaction could not be processed...<br><br>${error}`
                );
              }
            } ,
      }).render('#paypal-button-container').then(function(buttons) {
          paypalButtons = buttons; // Store the instance of PayPal buttons
      });

  }

  // Event listener for checkout button
  document.getElementById('checkout-btn').addEventListener('click', function() {
      var amount = parseInt(document.getElementById('total-amount').innerText);
      if (amount > 0) {
          initializePayPalButton(amount);
      } else {
          alert('Please enter a valid amount.');
      }
  });

  // Event listener for donation buttons
  beer3Button.addEventListener("click", () => {
      amountInput.value = 3;
      beerCount = 3;
      updateTotalAmount();
    });
    
    beer5Button.addEventListener("click", () => {
      amountInput.value = 5;
      beerCount = 5;
      updateTotalAmount();
    });
    
    beer10Button.addEventListener("click", () => {
      amountInput.value = 10;
      beerCount = 10;
      updateTotalAmount();
    });
    
    amountInput.addEventListener("input", () => {
      beerCount = amountInput.value;
      updateTotalAmount();
    });
    
    const updateTotalAmount = () => {
      const updatedAmount = beerCount * 50;
      totalAmount.innerText = updatedAmount;
    };
});