from django.shortcuts import render
import json
from django.http import JsonResponse
from .functions import create_order, capture_order

def index(request):
    return render(request, 'index.html')


def create_paypal_order(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            print('Received JSON data:', data)
            # value = request.POST.get('value')  
            order_value = data.get('value', [{}])[0].get('value', None)
            print(order_value)  
            print('Value!!:', data)
            response = create_order(order_value)
            return JsonResponse(response, safe=False) #value is not a dict , so safe=False is needed.
        except Exception as e:
            return JsonResponse({'error': str(e)})
    else:
        return JsonResponse({'error': 'Invalid request method'})


def capture_paypal_order(request):
    if request.method == 'POST':
        try:
            # order_id = request.POST.get('orderID')  # Adjust this based on your frontend data
            data = json.loads(request.body)
            order_id = data.get('orderID')  # Access 'orderID' from JSON data
            print('wwwwwwwww')
            print('order_id!!:', order_id)
            response = capture_order(order_id)
            return JsonResponse(response)
        except Exception as e:
            return JsonResponse({'error': str(e)})
    else:
        return JsonResponse({'error': 'Invalid request method'})
