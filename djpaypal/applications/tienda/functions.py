import base64
import requests


PAYPAL_CLIENT_ID = 'AXyeqSLjI0bTWbjKKCrxOtInsp3GVNT29N9eKpYq7B6IdR7oBSUfLhGbYw-mabPz4vATNbRx2I6XL0UM'  #AXyeqSLjI0bTWbjKKCrxOtInsp3GVNT29N9eKpYq7B6IdR7oBSUfLhGbYw-mabPz4vATNbRx2I6XL0UM
PAYPAL_CLIENT_SECRET = 'EG7MqEsbJmZkzDCX6rrMm-AEDbsvU4SF-gYy2zSBGvX6h-Zh0XkuigK0-0JMlBBiHzHOm7i6zsMF2UW2' #EG7MqEsbJmZkzDCX6rrMm-AEDbsvU4SF-gYy2zSBGvX6h-Zh0XkuigK0-0JMlBBiHzHOm7i6zsMF2UW2
BASE_URL = "https://api-m.sandbox.paypal.com"  # Test URL, in production use: https://api-m.paypal.com" (note:double check this!)
endpoint_token = "/v1/oauth2/token"
endpoint_order = "/v2/checkout/orders"



def generateAccessToken():
    if not PAYPAL_CLIENT_ID or not PAYPAL_CLIENT_SECRET:
        raise ValueError('no hay credenciales !!!')
    
    auth = f"{PAYPAL_CLIENT_ID}:{PAYPAL_CLIENT_SECRET}"   # PayPal uses this base64 format for its Tokens
    auth = base64.b64encode(auth.encode()).decode('utf-8')
    print(auth)
    
    response = requests.post(
        
        url = f"{BASE_URL}{endpoint_token}", 
        # url = "https://api-m.sandbox.paypal.com/v1/oauth2/token",
        data={"grant_type": "client_credentials"},
        headers={"Authorization": f"Basic {auth}"}
    )
    print('--- response ---', response.json())
    data = response.json()
    print('*********')
    print(data['access_token'])
    return data['access_token']


def create_order(value):
    try:
        access_token = generateAccessToken() 
        print("Value:", value)
        print(type(value))
        url = "https://api-m.sandbox.paypal.com/v2/checkout/orders"
        payload = {    #payload body shall have this structure
            "intent": "CAPTURE",
            "purchase_units": [
                {
                    "amount": {
                        "currency_code": "USD",  # this currency code must agree with that set in the frontend
                        "value": str(value)  # this asures that the value passed is a string, this value is the amount to be paid
                    }
                }
            ]
        }
        # payload = {
        #     "intent": "CAPTURE",
        #     "purchase_units": [
        #         {
        #              "amount": {
        #             "currency_code": "USD",  # Replace with your desired currency code
        #              "value": "100"  # Convert value to string in this example the total amount to pay is 100 USD
        #             }
        #         }
        #     ]
        # }
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {access_token}"
        }

        response = requests.post(url, headers=headers, json=payload) #  To retrieve the order_id from Paypal servers
        order_data = response.json()  
        
        if 'id' in order_data:
            print("Order_ID:", order_data)
            return order_data['id']
        else:
            raise Exception(f"Order creation failed: {order_data}")

    except Exception as error:
        print(error)
        raise error




def capture_order(orderID):
    access_token = generateAccessToken()
    url = f"https://api-m.sandbox.paypal.com/v2/checkout/orders/{orderID}/capture"
    
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {access_token}"
    }
    response = requests.post(url, headers=headers)
       
    return response.json()