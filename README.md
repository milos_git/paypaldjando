
# READ.md

-Simple Django project showing the basic usage of the PayPal SDK to make payments from a webpage (PayPal Checkout Buttons Integration)

The project relies on two files for the frontend (index.html and index.js) and two for the backend (views.py and functions.py which contains helper functions for the views.py file).










## Lessons Learned

To invoke and use the Paypal's SDK, particularly the use of PayPal checkout buttons.
The Data flow between Frontend and Backend in a PayPal's transaction.


## Appendix

Any additional information goes here


## Getting started

1.- Clone this repo.
    
2.- Create Virtual environment and Install  packages from the requirements.txt.
    pip install -r requirements.txt

3.- Get the PayPal required credentials to use PayPal SDK:

Go to https://developer.paypal.com/ and register an account.
 Then go to  https://developer.paypal.com/dashboard/accounts and obtain your ClientID and Secret Key foy your Sandbox test account.

 Notes:
 * Remember You can only make test transactions between test users.
 * Use sandbox API credentials for testing NOT TO Confuse   with: live credentials.
 

    
## Documentation

[Documentation](hhttps://developer.paypal.com/sdk/js/reference/, https://developer.paypal.com/demo/checkout/#/pattern/server, https://developer.paypal.com/studio/checkout/standard/integrate)



## Deployment

To deploy this project activate your virtual environment and execute: 
python manage.py runserver
From the djpaypal folder.


## Workflow explanation

let's break down how the frontend (index.html and index.js) and the backend (views.py and functions.py) work together. 

# Frontend Workflow (index.html and index.js)

HTML (index.html):

This file provides the structure and user interface (UI) elements for your payment page. It includes buttons for different donation amounts (donate-3, donate-5, donate-10), an input field (amount-input) for custom amounts, and a checkout button (checkout-btn).
The total amount to pay is displayed in total-amount.
*Remember to add the following code to invoke the PayPal SDK and render the payment buttons:
<script 
      src="https://www.paypal.com/sdk/js?client-id=test&currency=USD"  
      data-sdk-integration-source="developer-studio"
    ></script> 

 <div id="paypal-button-container">
    <!-- PayPal button will be rendered here -->
 </div>

 JavaScript (index.js):

Handles interactions and logic on the frontend.

Listen for clicks on donation buttons (donate-3, donate-5, donate-10) and the checkout button (checkout-btn).
Updates the total amount (total-amount) based on user input or button clicks.

Initializes PayPal buttons (initializePayPalButton(amount)) when the checkout button is clickedby setting up an event listener on this button (#checkout-btn) so when the button is clicked, it retrieves the total amount from an element (#total-amount), checks if the amount is greater than zero, and then calls a function (initializePayPalButton) to initialize PayPal payment buttons with the specified amount. If the amount is not valid (zero or less), it alerts the user to enter a valid amount. This uses PayPal's JavaScript SDK to create and render PayPal buttons dynamically (https://www.paypal.com/sdk/js).

Defines how to create a PayPal order when a user clicks the PayPal button (createOrder Function)

Handles what happens after a user approves the PayPal payment (onApprove Function) It sends an AJAX request (fetch('/capture_order/')) to the Django backend (views.py) to capture the order.


# Backend Workflow (views.py and functions.py)

views.py:

Contains Django views that handle HTTP requests from the frontend.

index Function: Renders the index.html template, which displays the payment page.

create_paypal_order Function:
Handles POST requests from the frontend to create a PayPal order. Retrieves the value from the request data and calls create_order function from functions.py.
Returns a JSON response with the PayPal order details.

capture_paypal_order Function:
Handles POST requests from the frontend to capture a PayPal order. Retrieves the orderID from the request data and calls capture_order function from functions.py.
Returns a JSON response with the capture status.

functions.py:

Contains functions responsible for interacting with the PayPal API.

generateAccessToken Function:
Generates an access token required for PayPal API authentication using client credentials.

create_order Function:
Creates a PayPal order using the provided amount (passed  by the Frontend)
Uses the access token obtained from generateAccessToken.
Returns the JSON response from the PayPal API.

capture_order Function:
Captures (completes) a PayPal order (/v2/checkout/orders/{orderID}/capture) using the provided orderID.
Uses the access token obtained from generateAccessToken.
Returns the JSON response from the PayPal API.

*Read the comments through the code




 

